# SQL basics 101

This is a repo to introduce SQL.

Objectives:

- Intro to SQL and DB
- Install mysql and workbench
- Define a table and columns

## Introduction to SQL and DB

What is a Database?

- A file with loads of information.
- Excel spreadsheet with contacts
- Recipies in a book
- Bunch of postit notes with emails and names.

These can all be considered "databases" but really they are not very organise or easy to use. They have limitation. In tech these would be what people are referring to as DBs

Usually in tech a DB is either:

- SQL or
- NoSQL ( not only SQL)

**SQL** (Structured Query Language)

... How is it structured?

It has structured tables with connection between tables. Imagine excel sheets connected to one another.

This makes it easy to query and get out the information you want.

### Common Terminology

**DB** Database
**SQL** Structure Query Language how a DB might be organised.
**DBMS** Database Management System

Inside a SQL DB, you will have:

**Columns**
**Rows**
**Tables**

# How do these connect 

BOOKS:

| title                  | author_id | date | price | genre     |
|------------------------|-----------|------|-------|-----------|
| Harry Potter           | 1         | 2001 | 12    | fiction   |
| Harry Potter 2         | 1         | 2003 | 12    | fiction   |
| Good Vibes - Good life | 2         | 2020 | 20    | Self Help |


AUTHORS:

| First_name | Last_name | email              |      
|------------|-----------|--------------------|
| J. K.      | Rowling   | jk@gmail.com       |      
| Harry      | Bookster  | hb@bookster.com    |  
| Vex        | King      | king@kingbooks.com |


GENRE:

| type      | description                                 | other_info |
|-----------|---------------------------------------------|------------|
| fiction   | these are fictional noval                   | none       |      
| Biography | some what true accounts of people's lives   | none       |      
| Selfhelp  | Meant to help you think and better yourself | none       |


We can see how the above connects because we are machines that see context in everything. But for a machine that is silicone based we need to use **Primary Keys** and **Foreign Keys**

**Primary Keys**



**Foreign keys**


Let's see the above example with primary keys:

| book_id  | title                  | author_id | date | price | genre     |
|----------|------------------------|-----------|------|-------|-----------|
| 1        | Harry Potter           | 1         | 2001 | 12    | fiction   |
| 2        | Harry Potter 2         | 1         | 2003 | 12    | fiction   |
| 3        | Good Vibes - Good life | 3         | 2020 | 20    | Self Help |


Authors

| author_id | First_name | Last_name | email              |   |   |
|-----------|------------|-----------|--------------------|---|---|
| 1         | J. K.      | Rowling   | jk@gmail.com       |   |   |
| 2         | Harry      | Bookster  | hb@bookster.com    |   |   |
| 3         | Vex        | King      | king@kingbooks.com |   |   |


### the keys create the Relations - One to One (1.1) 

### SQL Data types

There are different types. Charindex, Charmax, Decimals, integers.

We'll look at the difference.

### MySQL & Maria DB

Once you have install, enable and start mySQL or MariaDB

```bash
mysql -u root -p -h localhost
```

### DDL Data Definition Language

creating tables and columns

```SQL
-- Create a my_db

CREATE DATABASE <nameofDB>;

-- Create a table

CREATE TABLE film_table (
    film_name VARCHAR(60),
    release_date DATE,
    description VARCHAR(320)
);

```

In SQL we need to define the Datatypes for columns. The following exist:

- VARCHAR(n): Adaptable to different character lengths
- VARCHAR(max): Same as above with maximum length
- CHARACTER or CHAR: Always takes the exact size
- INT: Whole numbers (Integers)
- DATE, TIME, DATETIME
- DECIMAL OR NUMERIC: Defines the number of digits to the right of the decimal.
- BINARY: Used to story binary info such as pictures or music
- FLOAT: For very large numbers
- BIT: Similar to binary (0, 1, NULL)


### Modify/Change columns to 


```SQL

-- Modify 
ALTER TABLE film_table MODIFY COLUMN film_name VARCHAR(60) NOT NULL;

-- Change 
ALTER TABLE film_table CHANGE film_name film_name VARCHAR(60) NULL;

```

### Delete, Insert and Alter Rows


### Data Manipulation Language

Inserting, deleting and altering rows.

- INSERT INTO <table> VALUES (add info according to the table placements)
- DELETE FROM <table> WHERE <column> () [You can use LIKE to narrow search]
- UPDATE FROM <table> WHERE <column>

### Data Query Language


Syntax:

```SQL

SELECT <columns> FROM <table>;

-- Example get all customer data 

SELECT * FROM Customers;

-- I want from customer, the customer name, city and address.

SELECT CustomerName, Address,  FROM [Customers]

-- You can filter more by using key word 'WHERE'

-- You can also use LIKE (%) to narrow search. 


